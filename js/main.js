hide_tooltip();
function show_tooltip(id) {
	//var x = arr_cal[id];
	if ($(window).width() < 639) {
		if(id < 4) {
			left = 4;
			if(id == 0) {
				$('#tooltip_left_corner').css('visibility', 'visible');
			} else {
				new_x = id - 1;
				c_left = 16 + new_x*12 + new_x;
				$('#tooltip_center_corner').css({'left':c_left, 'top':'37px', 'visibility':'visible'}); 
			}
		} else if(id > 3 && id < 12) {
			new_x = id - 4;
			left = 7 + new_x*12 + new_x;			
			c_left = 35 + new_x*12 + new_x;
	        $('#tooltip_center_corner').css({'left':c_left, 'top':'37px', 'visibility':'visible'}); 
		} else if(id > 11 && id < 28) {
			new_x = id - 12;
			left = 32 + new_x*12 + new_x;	
			c_left = 81 + new_x*12 + new_x;
	        $('#tooltip_center_corner').css({'left':c_left, 'top':'37px', 'visibility':'visible'}); 
		} else if(id > 27 && id < 36) {
			new_x = id - 28;
			left = 168 + new_x*12 + new_x;	
			c_left = 198 + new_x*12 + new_x;
	        $('#tooltip_center_corner').css({'left':c_left, 'top':'37px', 'visibility':'visible'}); 
		} else if(id > 35) {
			left = 210;
			if(id != 39) {
				new_x = id - 36;
				c_left = 473 + new_x*12 + new_x;
	            $('#tooltip_center_corner').css({'left':c_left, 'top':'37px', 'visibility':'visible'}); 
			} else {
				$('#tooltip_right_corner').css('visibility', 'visible');
			}				
		}
		$('#tooltip_cal').css({'width':'98px', 'left':left, 'top':'46px', 'visibility':'visible'}).html('<div class="week_name">' + (id+1) + ' неделя</div>'); 
	}else{

		if(id < 4) {
			left = 7;
			if(id == 0) {
				$('#tooltip_left_corner').css('visibility', 'visible');
			} else {
				new_x = id - 1;
				c_left = 16 + new_x*12 + new_x;
				$('#tooltip_center_corner').css({'left':c_left, 'top':'37px', 'visibility':'visible'}); 
			}
		} else if(id > 3 && id < 12) {
			new_x = id - 4;
			left = 13 + new_x*12 + new_x;			
			c_left = 55 + new_x*12 + new_x;
	        $('#tooltip_center_corner').css({'left':c_left, 'top':'37px', 'visibility':'visible'}); 
		} else if(id > 11 && id < 28) {
			new_x = id - 12;
			left = 117 + new_x*12 + new_x;	
			c_left = 159 + new_x*12 + new_x;
	        $('#tooltip_center_corner').css({'left':c_left, 'top':'37px', 'visibility':'visible'}); 
		} else if(id > 27 && id < 36) {
			new_x = id - 28;
			left = 327 + new_x*12 + new_x;	
			c_left = 369 + new_x*12 + new_x;
	        $('#tooltip_center_corner').css({'left':c_left, 'top':'37px', 'visibility':'visible'}); 
		} else if(id > 35) {
			left = 419;
			if(id != 39) {
				new_x = id - 36;
				c_left = 473 + new_x*12 + new_x;
	            $('#tooltip_center_corner').css({'left':c_left, 'top':'37px', 'visibility':'visible'}); 
			} else {
				$('#tooltip_right_corner').css('visibility', 'visible');
			}				
		}
		$('#tooltip_cal').css({'width':'98px', 'left':left, 'top':'46px', 'visibility':'visible'}).html('<div class="week_name">' + (id+1) + ' неделя</div>'); 
		function hide_tooltip() {
			$('#tooltip_cal').css({'visibility':'visible', 'width':'255px', 'left':'130px', 'top':'46px'}).html('<div class="week_name">Выберите неделю беременности</div>');
			$('#tooltip_left_corner, #tooltip_center_corner, #tooltip_right_corner').css('visibility', 'hidden'); 
		}
	}

};
function toggleCommentReplies(id, eventSource)
{
	var $comments = $('#cr'+id)
	$(eventSource).text($comments.is(':hidden')?'Закрыть ветку':'Открыть ветку')
	$comments.animate({'height':'toggle'},200);
}
function hide_tooltip() {
	if ($(window).width() < 639) {
	$('#tooltip_cal').css({'visibility':'visible', 'width':'255px', 'left':'60px', 'top':'46px'}).html('<div class="week_name">Выберите неделю беременности</div>');
	}else{
	$('#tooltip_cal').css({'visibility':'visible', 'width':'255px', 'left':'130px', 'top':'46px'}).html('<div class="week_name">Выберите неделю беременности</div>');
	}
	$('#tooltip_left_corner, #tooltip_center_corner, #tooltip_right_corner').css('visibility', 'hidden'); 
}
$(".marks_cloud").mouseenter(function () {
		var $hidden = $("a.hidden", $(this));
		$hidden.stop();
		$hidden.css({opacity: 0.1, display: "inline-block"}).animate({opacity: 1.0}, 200);
	}).mouseleave(function () {
		var $hidden = $("a.hidden", $(this));
		$hidden.stop();
		$hidden.animate({opacity: 0.0}, 200, function() {
			$(this).hide();
		});
	});



	var clearingSearch = false;

	function nameSearchFormSubmit() {
		sendForm("nameSearchForm", "namesPageContent", false, true, false, false, function() {
			var foundCount = $('[id^="name-"]').length;
			$("#foundCount").text("Найдено имён: " + foundCount);

			if (!clearingSearch) {
				$(".clearSearch").show();
			}

			clearingSearch = false;

		}, false);
		return false;
	}


	$(document).ready(function() {
		$(".namesSexTab").click(function() {
			var $form = $(".search-area-all");
			var $link = $(this);
			var sex = $link.data("sex");
			if (!$link.hasClass("active")) {
				$(".namesSexTab").removeClass("active");
				$link.addClass("active");

				$form.animate({opacity: 0.1}, 10, function() {
					$form.attr("data-sex", sex);
					$form.animate({opacity: 1.0}, 10);
				});

				// set_ac_sex(sex);
				
				if (window.father_autocomplete) {
					var suggestions = window.father_autocomplete.suggestions;
					if (suggestions.length > 0) {
						var found = false;
						for (var i = 0; i < suggestions.length; i++) {
							if ($("#father").val().toUpperCase() == suggestions[i].toUpperCase()) {
								found = true;
								break;
							}
						}

						// if name is not in suggested list then clear father name
						if (!found) {
							$("#father").val("");
						}
					}
					window.father_autocomplete.hide();
				}

				$("input[name='radio']").val(sex);
				// $("#nameSearchForm").submit();
			}

			return false;
		});

		$(".clearSearch").click(function() {
			clearingSearch = true;

			$("#child, #father").val("");
			$("#zodiac, #month").val("none");

			if ($("#radio").val() != "any") {
				//noinspection JSUnusedAssignment
				$(".namesSexTabAll").trigger("click");
			} else {
				$("#nameSearchForm").submit();
			}

			$(this).hide();

			return false;
		});

		var owl = $("#owl-demo");
		owl.owlCarousel({
		    items : 10, //10 items above 1000px browser width
		    itemsDesktop : [1000,5], //5 items between 1000px and 901px
		    itemsDesktopSmall : [900,3], // betweem 900px and 601px
		    itemsTablet: [600,2], //2 items between 600 and 0
		    itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
		});
		
		// Custom Navigation Events
		$(".next").click(function(){
		  owl.trigger('owl.next');
		})
		$(".prev").click(function(){
		  owl.trigger('owl.prev');
		})
		$(".play").click(function(){
		  owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
		})
		$(".stop").click(function(){
		  owl.trigger('owl.stop');
		})
	});

$(document).ready(function() {
 		var owlTop = $("#owl-top");
		owlTop.owlCarousel({
		    items : 10, //10 items above 1000px browser width
		    itemsDesktop : [1000,5], //5 items between 1000px and 901px
		    itemsDesktopSmall : [900,3], // betweem 900px and 601px
		    itemsTablet: [600,2], //2 items between 600 and 0
		    itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
		});
		
		// Custom Navigation Events
		$(".next-top").click(function(){
		  owlTop.trigger('owl.next');
		})
		$(".prev-top").click(function(){
		  owlTop.trigger('owl.prev');
		})
		$(".play-top").click(function(){
		  owlTop.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
		})
		$(".stop-top").click(function(){
		  owlTop.trigger('owl.stop');
		})

 		var owlBottom = $("#owl-bottom");
		owlBottom.owlCarousel({
		    items : 10, //10 items above 1000px browser width
		    itemsDesktop : [1000,5], //5 items between 1000px and 901px
		    itemsDesktopSmall : [900,3], // betweem 900px and 601px
		    itemsTablet: [600,2], //2 items between 600 and 0
		    itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
		});
		
		// Custom Navigation Events
		$(".next-bottom").click(function(){
		  owlBottom.trigger('owl.next');
		})
		$(".prev-bottom").click(function(){
		  owlBottom.trigger('owl.prev');
		})
		$(".play-bottom").click(function(){
		  owlBottom.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
		})
		$(".stop-bottom").click(function(){
		  owlBottom.trigger('owl.stop');
		}) 
 
});

$(document).ready(function() {
		$('a').on('click',function(e) {
		    if ($(this).hasClass('gridview')) {
		        $('.title-block__row').removeClass('cart list').addClass('plitka');
		        $('.title-block__yandex-map').removeClass('vissible').addClass('hidden');
		        $('.pagination-block').removeClass('hidden').addClass('vissible');
		        $('.block-for-list-item').removeClass('vissible').addClass('hidden');
		        $(this).addClass('active');
		        $('.listview').removeClass('active');
		        $('.cartview').removeClass('active');
		    }
		    if ($(this).hasClass('listview')) {
		        $('.title-block__row').removeClass('cart plitka').addClass('list');
		        $('.title-block__yandex-map').removeClass('vissible').addClass('hidden');
		        $('.pagination-block').removeClass('hidden').addClass('vissible');
		        $('.block-for-list-item').removeClass('hidden').addClass('vissible');
		        $(this).addClass('active');
		        $('.gridview').removeClass('active');
		        $('.cartview').removeClass('active');
		    }
		    else if($(this).hasClass('cartview')) {
		        $('.title-block__row').removeClass('list plitka').addClass('cart');
		        $('.title-block__yandex-map').removeClass('hidden').addClass('vissible');
		        $('.pagination-block').removeClass('vissible').addClass('hidden');
		        $('.block-for-list-item').removeClass('vissible').addClass('hidden');
		        $(this).addClass('active');
		        $('.gridview').removeClass('active');
		        $('.listview').removeClass('active');
		    }
		});

});

$(document).ready(function() {

	$('.popup-one .close_window, .overlay-one').click(function (){
		$('.popup-one, .overlay-one').css({'opacity': 0, 'visibility': 'hidden'});
	});
	$('.open_window').click(function (e){
		$('.popup-one, .overlay-one').css({'opacity': 1, 'visibility': 'visible'});
		e.preventDefault();
	});

	$('.popup-two .close_window, .overlay-two').click(function (){
		$('.popup-two, .overlay-two').css({'opacity': 0, 'visibility': 'hidden'});
	});
	$('.open_windowtwo').click(function (e){
		$('.popup-two, .overlay-two').css({'opacity': 1, 'visibility': 'visible'});
		e.preventDefault();
	});

});

